import {useForm} from 'react-hook-form'
import { loginUser } from '../../api/user';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'
import { useState, useEffect } from 'react';
import {useNavigate} from 'react-router-dom'
import { storageSave } from '../../utils/storage';
import { useUser } from '../../context/UserContext';
import {STORAGE_KEY_USER} from  '../../const/storageKeys'



// Crate user object outside of the component and make requirements
const usernameConfig = {

    required: true,
    minLength: 3
}
// Create a use form from react-hook-form to handle the submit
const LoginForm = () =>{

    // Hooks

    // Activate from usForm
    const { register, handleSubmit, formState: {errors} } = useForm();
    
    const {user, setUser} = useUser();
    const navigate = useNavigate();
    

    /**Local State */

    // Loading state to avoid multiple request 
   const [loading, setLoading] = useState(false)
   // API error message
   const [apiError, setApiError] = useState(null);

     /**Side Effects*/
   
     useEffect(() =>{
         if(user !== null){

            navigate('translations')
         }
         console.log('User has changed!', user)
     }, [user, navigate])

    /**Event Handlers*/

    // Event handler for submit
    const onSubmit = async ({username}) => {

        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if(error !==null){

            setApiError(error);
        }

        if(userResponse !== null){

            storageSave(STORAGE_KEY_USER, userResponse);
            setUser(userResponse);
        }

        setLoading(false);

    }

      /**Render Functions */

    const errorMessage = (() =>{

        // Check of the username is null
        if(!errors.username){

            return null;
        }
        // Check of the username is empty
        if(errors.username.type === 'required'){

           return <span>Username is required</span>
        }
        // Check the minimum required of the username
        if(errors.username.type === 'minLength'){
            return <span>Username is too short (min 3)</span>
        }
    }) ()

    return(

        // React fragment
        <>
        <h1>What is your name?</h1>
        <Form className="mb-3" onSubmit={handleSubmit(onSubmit)}>

            <fieldset>
                <label htmlFor="username">Username</label>
                <input type="text"
                placeholder='What is your name?'
                {...register("username",usernameConfig)} />
                {errorMessage}
            </fieldset>
            <Button type="submit" disabled={ loading } variant="primary">Continue</Button>
            {loading && <p>Loggin in...</p>}
            {apiError && <p>{apiError}</p>}
        </Form>
        </>
    )
}

export default LoginForm;