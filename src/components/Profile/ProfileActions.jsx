import { Link } from "react-router-dom"
import { translationClearHistory } from "../../api/translations";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";

const ProfileActions = () =>{


    // Access to current user
    const {user, setUser} = useUser()

    // Handle logout
    const handleLogoutClick =()=>{
        if(window.confirm("Do you want to logout?")){
            // Send event to the parent
            storageDelete(STORAGE_KEY_USER, null)
            setUser(null)
        }


    }
    // Clear history
    const handleClearHistoryClick = async()=>{
        if(!window.confirm("Do you want to clear the history?")){
            return;
        }

        const [clearError] = await translationClearHistory(user.id)
        
        if(clearError !==null){
            //Do something about this
            return
        }

        const updateUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER,updateUser)
        setUser(updateUser)
    }


    return(
        <ul>
            <li><button onClick={handleClearHistoryClick}>Clear history</button></li>
            <li><button onClick={handleLogoutClick}>Logout</button></li>
        </ul>
    )

}

export default ProfileActions