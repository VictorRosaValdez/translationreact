import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext";


const Navbar = ()=>{

    // To show the navbar only when the are a user
    const {user} = useUser();

    return(
    
    <nav>
        <ul>
            <li>Translations</li>
        </ul>
        {
            // This is a parent element. We can check this without if statement
            user !== null &&

        <ul>
            <li>
                <NavLink to="/translations">Translations</NavLink>
            </li>
        
            <li>
                <NavLink to="/profile">Profile</NavLink>
            </li>
        </ul>

        }


    </nav>)
}

export default Navbar