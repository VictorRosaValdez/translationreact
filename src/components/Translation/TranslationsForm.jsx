
import { useState } from 'react'
import {useForm} from 'react-hook-form'
import { translationAdd } from '../../api/translations'
import { useUser } from '../../context/UserContext'


   // Array of Signs
   const signs = [ 
   {id:"a",image:"img/a.png"},
   {id:"b",image:"img/b.png"},
   {id:"c",image:"img/c.png"},
   {id:"d",image:"img/d.png"},
   {id:"e",image:"img/e.png"},
   {id:"f",image:"img/f.png"},
   {id:"g",image:"img/g.png"},
   {id:"h",image:"img/h.png"},
   {id:"i",image:"img/i.png"},
   {id:"j",image:"img/j.png"},
   {id:"k",image:"img/k.png"},
   {id:"l",image:"img/l.png"},
   {id:"m",image:"img/m.png"},
   {id:"n",image:"img/n.png"},
   {id:"o",image:"img/o.png"},
   {id:"p",image:"img/p.png"},
   {id:"q",image:"img/q.png"},
   {id:"r",image:"img/r.png"},
   {id:"s",image:"img/s.png"},
   {id:"t",image:"img/t.png"},
   {id:"u",image:"img/u.png"},
   {id:"v",image:"img/v.png"},
   {id:"w",image:"img/w.png"},
   {id:"x",image:"img/x.png"},
   {id:"y",image:"img/y.png"},
   {id:"z",image:"img/z.png"},

   
]

let signsTranslation =[];


const TranslationsForm = (splitTranslation)=>{


    const {register, handleSubmit} =useForm()
    const [setTranslation] = useState(null)
    const {user} = useUser();

    // Event
    const onSubmit = async (data) =>{
    
         // Get the value of object translation
         const translation = Object.values(data)

         //let stringTranslation = translation.toString();
         let splitTranslation = translation.toString().split('')


    
         signsTranslation =[]
         //translateSentence(data);

        
        for (let i = 0; i < splitTranslation.length; i++) {
            let element = splitTranslation[i]
            signsTranslation.push(<img 
                key={signs[i].id} 
                src={signs[i].image} 
                alt={signs[i].id} 
                width='55'/>);
        
        }
        
         // Is not working as expect. 
        if(!translation){

            alert("Please make a translation")
            return;
        }
    

        const [error, result] = await translationAdd(user, translation)
    

    }
    return(
        <>
         
        <form onSubmit={handleSubmit(onSubmit)} className="mb-3">
        <fieldset>
        <label htmlFor="translation">Translation</label>
        <input type="text" {...register('translation')}
         placeholder='Translate word' />
      
        </fieldset>
        <button type='submit'>Translate</button>
        {signsTranslation}
        </form>
        
        </>
        
    )

}

export default TranslationsForm;