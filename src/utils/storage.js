
// Validate key
const ValidateKey = key =>{
  if(!key || typeof key !=='string'){
    throw new Error ('Invalid storage key provided')
  }
}

// Storage the user on the local storage
export const storageSave = (key, value) => {
  

  ValidateKey(key);

  if(!value){
    throw new Error ('storageSave: No value provided for key')
  }

   
  localStorage.setItem(key, JSON.stringify(value));
  };
  
  // Save data as a JS object
  export const storageRead = (key) => {
  

    ValidateKey(key);

      const data = localStorage.getItem(key)
  
      if(data){
  
          return JSON.parse(data);
      }
  
      return null;
  }
  
  export const storageDelete = key =>{

    ValidateKey(key);

    localStorage.removeItem(key)
  }
  