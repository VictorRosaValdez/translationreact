import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationsHistory from "../components/Profile/ProfileTranslationsHistory";
import TranslationsForm from "../components/Translation/TranslationsForm";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth"

const Profile = () => {

    // Access to current user
    const {user} = useUser()
    

    return(

        <>
        <h1>Profile </h1>
        <ProfileHeader username={user.username}/>
        <ProfileActions/>
        <ProfileTranslationsHistory translations={user.translations}/>
        <section id="translation">
        </section>
        </>
        
        
    )
}
//Calling the function to protect the profile
export default withAuth(Profile);