import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"
import ProfileHeader from "../components/Profile/ProfileHeader"
import TranslationsForm from "../components/Translation/TranslationsForm"
import ProfileActions from "../components/Profile/ProfileActions"

const Translations = () => {
    
      // Access to current user
      const {user} = useUser()


    return(

        <>
        <h1> Translations </h1>
        <ProfileHeader username={user.username}/>
        <ProfileActions/>
        <TranslationsForm />
        </>
    )
}
//Calling the function to protect the profile
export default withAuth(Translations);