import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

// Higher-Order to protect the profile route
const withAuth = Component => props =>{

    const {user} = useUser()
    if (user !==null) {
        
        return <Component {...props} />
    }
    else{

        return <Navigate to="/" />
    }
}

export default withAuth