import './App.css';
import {
  BrowserRouter,
  Routes,
  Route} from 'react-router-dom'

import Login from './pages/Login';
import Profile from './pages/Profile';
import Translations from './pages/Translations';
import logo from './logo.png'
import Navbar from './components/Navbar/Navbar' 

function App() {


  return (
    <BrowserRouter>
    <div className="App back-color">
    <Navbar/>
       <Routes>
        <Route path="/" element ={<Login/>} />
        <Route path="/translations" element ={<Translations/>} />
        <Route path="/profile" element ={<Profile/>} />
        
      </Routes>
    </div>
    <img src={logo} className="App-logo" alt="logo" />
   </BrowserRouter>
  );
}

export default App;
